import pandas as pd
import numpy as np
from sklearn import linear_model
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.ensemble import RandomForestRegressor
from geopy.distance import vincenty
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split


def vincenty_vec(vec_coord):
    vin_vec_dist = np.zeros(vec_coord.shape[0])
    if vec_coord.shape[1] != 4:
        print("ERROR: Bad number of columns (shall be = 4)")
    else:
        vin_vec_dist = [vincenty(vec_coord[m, 0:2], vec_coord[m, 2:]).meters
                        for m in range(vec_coord.shape[0])]
    return vin_vec_dist


# evaluate distance error for each predicted point
def Eval_geoloc(y_train_lat, y_train_lng, y_pred_lat, y_pred_lng):
    vec_coord = np.array([y_train_lat, y_train_lng, y_pred_lat, y_pred_lng])
    err_vec = vincenty_vec(np.transpose(vec_coord))
    return err_vec


mess_train = pd.read_csv("mess_train_list.csv")
pos_train = pd.read_csv("pos_train_list.csv")
mess_test = pd.read_csv("mess_test_list.csv")

base_list = np.union1d(np.unique(mess_train['bsid']),
                       np.unique(mess_test['bsid']))


temp = mess_train[["objid", "bsid"]]
temp["bs_lat"] = mess_train["bs_lat"]
features_lat = temp.pivot(index="objid", columns="bsid", values="bs_lat")
features_lat.fillna(0, inplace=True)

temp = mess_train[["objid", "bsid"]]
temp["bs_lng"] = mess_train["bs_lng"]
features_lng = temp.pivot(index="objid", columns="bsid", values="bs_lng")
features_lng.fillna(0, inplace=True)

x_lat = features_lat.values
x_lng = features_lng.values
temp_y = pd.concat([mess_train, pos_train], axis=1)
temp_y = temp_y[["objid", "lat", "lng"]]
y = temp_y.groupby("objid").mean()
y_lat = y["lat"].values
y_lng = y["lng"].values

train_x_lat, val_x_lat, train_y_lat, val_y_lat = train_test_split(x_lat, y_lat, test_size=0.20)
train_x_lng, val_x_lng, train_y_lng, val_y_lng = train_test_split(x_lng, y_lng, test_size=0.20)

lr = linear_model.LinearRegression()
lr.fit(train_x_lat, train_y_lat)
lr_predictions_lat = lr.predict(val_x_lat)
lr.fit(train_x_lng, train_y_lng)
lr_predictions_lng = lr.predict(val_x_lng)

lr_err_vec = Eval_geoloc(val_y_lat, val_y_lng, lr_predictions_lat, lr_predictions_lng)
lr_perc_error = np.percentile(lr_err_vec, 80)
print("linear regression : ", lr_perc_error)

rfr = RandomForestRegressor()
rfr.fit(train_x_lat, train_y_lat)
rfr_predictions_lat = rfr.predict(val_x_lat)
rfr.fit(train_x_lng, train_y_lng)
rfr_predictions_lng = rfr.predict(val_x_lng)

rfr_err_vec = Eval_geoloc(val_y_lat, val_y_lng, rfr_predictions_lat, rfr_predictions_lng)
rfr_perc_error = np.percentile(rfr_err_vec, 80)
print("random forest : ", rfr_perc_error)

et = ExtraTreesRegressor()
et.fit(train_x_lat, train_y_lat)
et_predictions_lat = et.predict(val_x_lat)
et.fit(train_x_lng, train_y_lng)
et_predictions_lng = et.predict(val_x_lng)

et_err_vec = Eval_geoloc(val_y_lat, val_y_lng, et_predictions_lat, et_predictions_lng)
et_perc_error = np.percentile(et_err_vec, 80)
print("extra trees : ", et_perc_error)

